
# This script is written by Andr�s Botero
# This script is provided under the ZLIB license

# This script aims to set up an environment in powershell
# Like "C:/Program Files/Microsoft Visual Studio/2017/Community/Common7/Tools/vsdevcmd.bat does
# Some (a lot?) of values are hardcoded, I am not planning on fixing them now





# start vsdevcmd_start

# start parse_cmd.bat

Param(
	[string] $TargetArch, # can be x86, x64, arm, arm64
	[string] $HostArch, # if empty, we use TargetArch
	[string] $Platform = 'Desktop', # can also be 'UWP' or 'OneCore'
	[string] $WinSDK, # if empty, handle later
	[switch] $Clean = $false,
	[switch] $Test = $false
	)

# TODO test if platform is x86 or x64
if ($TargetArch -eq ''){
   $TargetArch = 'x86'
   if (${env:ProgramFiles(x86)}){ $TargetArch = "x64" }
}

if ($HostArch -eq '') {
   $HostArch = $TargetArch
}

if ($HostArch -eq $TargetArch) {
   $env:CommandPromptType = "Native"
}
else {
     $env:CommandPromptType = "Cross"
}


# end parse_cmd.bat

if ($Test) {
   Write-Host "TODO: Handle Tests"
   # or maybe call the visual VsDevCmd tests?
}

# TODO this path is for the VS build tools when installed apart from a
# Visual studio IDE installation, we have to do some checks here
$program_files = $env:ProgramFiles
if (${env:programFiles(x86)}) { $program_files = ${env:ProgramFiles(x86)} }

$vs_installations = $program_files + '\Microsoft Visual Studio\2017\'

$installation = Get-Item "$vs_installations/BuildTools" -ErrorAction SilentlyContinue
if ($community = Get-Item "$vs_installations/Community" -ErrorAction SilentlyContinue) {
   $installation = $community
}
# maybe add enterprise?

if (-Not $installation){
   Write-Host "can't find a visual studio installation"
   Exit
}


$env:vsinstalldir = $installation.FullName + '\'

$env:DevEnvDir = $env:vsinstalldir + 'Common7\IDE\'
$env:VS150COMNTOOLS = $env:vsinstalldir + 'Common7\Tools\'

# end vsdevcmd_start.bat
# TODO find out this version
$env:VisualStudioVersion = '15.0'

$env:VSCMD_ARG_app_plat = $Platform
$env:VSCMD_ARG_HOST_ARCH = $HostArch
$env:VSCMD_ARG_TGT_ARCH = $TargetArch
$env:VSCMD_VER = $env:VisualStudioVersion



# start msbuild.bat

$msbuild_path = $env:vsinstalldir + "\MSBuild\15.0\bin"
$env:PATH = "$msbuild_path;$env:DevEnvDir;$env:VS150COMNTOOLS;$env:PATH"

#end msbuild.bat

# start winsdk.bat
# actually this is something simplified as I am not interested in w8.1 sdk

if ($WinSDK -ne 'none') {
   # reg query "HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v10.0" /v "InstallationFolder"
   # hardcoded
   $env:WindowsSDKDir = "$program_files\Windows Kits\10\"
   
   # if empty, find latest, otherwise use specified
   # hardcoded for now
   
   $env:WindowsSDKVersion = '10.0.17763.0'
   $env:WindowsSDKLibVersion=$env:WindowsSDKVersion

   $env:WindowsSDKBinPath = $env:WindowsSDKDir + 'bin\'
   $env:WindowsSDKVerBinPath = $env:WindowsSDKBinPath + $env:WindowsSDKVersion + '\'
   $env:WindowsLibPath = $env:WindowsSDKDir + 'UnionMetadata\' + $env:WindowsSDKVersion
   $env:WindowsLibPath += ';' + $env:WindowsSDKDir + 'References\' + $env:WindowsSDKVersion

   # reg query 'HKLM\SOFTWARE\Microsoft\Windows Kits\Installed Roots' /v 'KitsRoot10'
   $env:UniversalCRTSdkDir = "$program_files\Windows Kits\10\"
   # now read in include folder inside universalcrtsdkdir and then:
   $env:UCRTVersion = '10.0.17763.0'

   # TODO check host arch
   $sdk_path = $env:WindowsSDKBinPath + $HostArch
   $sdk_path = $env:WindowsSDKVerBinPath + $HostArch + ";$sdk_path"

   # TODO check target arch

   $Lib = $env:WindowsSDKDir + 'lib\' + $env:WindowsSDKLibVersion + '\um\' + $TargetArch + ';'

   $baseInclude = $env:WindowsSDKDir + 'include\' + $env:WindowsSDKVersion

   $include = $include + $baseInclude + '\shared;'
   $include = $include + $baseInclude + '\um;'
   $include = $include + $baseInclude + '\winrt;'
   $include = $include + $baseInclude + '\cppwinrt'

   $libpath = $env:WindowsLibPath + ';' + $env:libpath

   $include = $env:UniversalCRTSdkDir + 'include\' + $env:UCRTVersion + '\ucrt;' + $include
   $lib = $env:UniversalCRTSdkDir + 'lib\' + $env:UCRTVersion + '\ucrt\'+$TargetArch+';' + $lib

   $env:WindowsSDKVersion += '\'
   $env:WindowsSDKLibVersion += '\'

   $env:INCLUDE = $include
   $env:LIB = $lib
   $env:LIBPATH = $libpath
   $env:PATH = $sdk_path + ';' + $env:PATH
}

# end winsdk.bat

# this does not include the dotnet.bat file in the tools/core directory
# or the cmake.bat, netfxsdk.bat, roslyn.bat, testwindow.bat, vcvars.bat
# placed in the tools/ext directory


# start vcvars.bat

$env:VCInstallDir = $env:vsinstalldir + 'VC\'
$env:VCIDEInstallDir = $env:vsinstalldir + 'Common7\IDE\VC\'

# @if exist "%ProgramFiles%\Microsoft SDKs\Windows Kits\10\ExtensionSDKs"

$env:ExtensionSdkDir=$program_files + '\Microsoft SDKs\Windows Kits\10\ExtensionSDKs'
# or use $programfilesx86


$VCConfigFile = $env:VCInstallDir + 'Auxiliary\Build\Microsoft.VCToolsVersion.default.txt'

$env:VCToolsVersion = Get-Content $VCConfigFile

$env:VCToolsInstallDir = $env:VCInstallDir + 'Tools\MSVC\'+ $env:VCToolsVersion +'\'

$vctools_path = $env:VCToolsInstallDir + 'bin\host' + $HostArch + '\' + $TargetArch + ';'
$vcpackages_path = $env:VCIDEInstallDir + "VCPackages;"
$env:PATH = $vctools_path + $vcpackages_path + $env:PATH

$VCRedistFile = $env:VCInstallDir + 'Auxiliary\Build\Microsoft.VCRedistVersion.default.txt'
$VCRedistVersion = Get-Content $VCRedistFile

$env:VCToolsRedistDir = $env:VCInstallDir + 'Redist\MSVC\' + $VCRedistVersion + '\'

$env:INCLUDE = $env:VCToolsInstallDir + "include;$env:INCLUDE"
$env:INCLUDE = $env:VCToolsInstallDir + "ATLMFC\include;$env:INCLUDE"
# I don't feel like adding ATLMFC every time

if ($Platform -eq "Desktop") {
   $env:LIB = $env:VCToolsInstallDir + "lib\" + $TargetArch + ";$env:LIB"
   $env:LIB = $env:VCToolsInstallDir + "ATLMFC\lib\" + $TargetArch + ";$env:LIB"
   $env:LIBPATH = $env:VCToolsInstallDir + "lib\" + $TargetArch + "\store\references;$env:LIBPATH"
   $env:LIBPATH = $env:VCToolsInstallDir + "lib\" + $TargetArch + ";$env:LIBPATH"
   $env:LIBPATH = $env:VCToolsInstallDir + "ATLMFC\lib\" + $TargetArch + ";$env:LIBPATH"
}

if ($Platform -eq "UWP") { # untested
   $env:LIB = $env:VCToolsInstallDir + "lib\" + $TargetArch + "\store\;$env:LIB"
   $env:LIBPATH = $env:ExtensionSDKDir + "\Microsoft.VCLibs\14.0\References\CommonConfiguration\neutral;$env:LIBPATH"
}
if ($Platform -eq "OneCore") { # untested, spectre stuff left out
   $env:LIB = $env:VCToolsInstallDir + "lib\onecore\" + $TargetArch + "\store\;$env:LIB"
   $env:LIBPATH = $env:VCToolsInstallDir + "lib\onecore\" +$TargetArch + ";$env:LIBPATH"
}


Write-Host "The VS environment has been set up"
Write-Host "This script only sets the environment for C/C++ dev, not C#."
Write-Host "This script is not guaranteed to work"
Write-Host "Always remember you can set a powershell vs environment following this guide"
Write-Host "https://stackoverflow.com/questions/2124753/how-can-i-use-powershell-with-the-visual-studio-command-prompt"


